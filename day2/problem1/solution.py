with open("input.txt", "r") as f:
    data = f.read().strip()
stringintcode = data.split(",")
intcode = list(map(int, stringintcode))
currentIndex = 0
while intcode[currentIndex] != 99:
    if intcode[currentIndex] == 1:
        intcode[intcode[currentIndex+3]]=intcode[intcode[currentIndex+1]] + intcode[intcode[currentIndex+2]]
    if intcode[currentIndex] == 2:
        intcode[intcode[currentIndex+3]]=intcode[intcode[currentIndex+1]] * intcode[intcode[currentIndex+2]]
    currentIndex += 4
print(intcode)
