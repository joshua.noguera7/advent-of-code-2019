with open("input.txt", "r") as f:
    data = f.read().strip()
basememory = list(map(int, data.split(",")))
def compute(intcode):
    currentIndex = 0
    while intcode[currentIndex] != 99:
        if intcode[currentIndex] == 1:
            intcode[intcode[currentIndex+3]]=intcode[intcode[currentIndex+1]] + intcode[intcode[currentIndex+2]]
        if intcode[currentIndex] == 2:
            intcode[intcode[currentIndex+3]]=intcode[intcode[currentIndex+1]] * intcode[intcode[currentIndex+2]]
        currentIndex += 4
    return intcode[0]

for i in range(100):
    for j in range(100):
        testmemory = list(basememory)
        testmemory[1] = i
        testmemory[2] = j
        try:
            if compute(testmemory) == 19690720:
                print(100*i+j)
        except IndexError:
            pass

