def calculateFuel(mass):
    if mass > 8:
        return int(mass/3)-2+calculateFuel(int(mass/3)-2)
    else:
        return 0

with open("input.txt", "r") as f:
    total = 0
    for line in f:
        total += calculateFuel(int(line))
    print(total)
